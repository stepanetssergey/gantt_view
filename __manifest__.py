{
    'name': "Gantt view module",
    'version': '1.0',
    'depends': ['base',
                'web',
                'sale',
                'portal',
                'account',
                'project',
                'base_setup', 
                'bus'
],
    'author': "Sergey Stepanets",
    'category': 'Application',
    'description': """
    Gantt view module
    """,
    'data': [
    #  'views/web/gantt.xml',
    'views/web/gantt_test.xml',
    'views/task.xml',
    'views/project.xml',
    ],
}