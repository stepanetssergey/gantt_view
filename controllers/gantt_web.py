# -*- coding: utf-8 -*-
import json
import logging
import werkzeug.utils
import time
import datetime

from odoo import http
from odoo.http import request
from odoo.release import description
from odoo.addons.bus.controllers.main import BusController
from odoo.addons.test_documentation_examples import inheritance

_logger = logging.getLogger(__name__)


#class GanttController(http.Controller):
class GanttController(BusController):
    
    def _poll(self, dbname, channels, last, options):
        print('test polling:',dbname, channels, last, options)
        """Add the relevant channels to the BusController polling."""
        if options.get('project.task'):
            channels = list(channels)
            ticket_channel = (
                request.db,
                'project.task',
                options.get('project.task')
            )
            channels.append(ticket_channel)
        return super(GanttController, self)._poll(dbname, channels, last, options)

    @http.route('/gantt/<int:project_id>', type='http', auth='public')
    def gantt_web(self, project_id,debug=False, **k):
        context = {}
        res_company = request.env['res.company'].sudo().search([])
        context = {'res_company':res_company,
                   'project_id':project_id}
        print('test of context',context)
        return request.render('gantt_view.index', qcontext=context)
    
    def get_number_of_depends(self,id,task_list):
        for item_depends in task_list:
            get_task_id = request.env['project.task.predecessor'].search([('id','=',id)])
            if item_depends['id'] == get_task_id.parent_task_id.id:
                return item_depends['number']
    
    def write_predecessors_number(self,item,depends_dict):
        predecessors_gantt_list = []
        predecessors_list = []
        return_for_write = []
        predecessors_list_related_model = request.env['project.task.predecessor'].search([('task_id','=',item['id'])])
        if item['depends'] != "":
            for item_depends in item['depends'].split(','):
                predecessors_gantt_list.append(depends_dict[item_depends])                
        #change id of predecessor one2many to id of task in predecessors_list
        for item_predecesor_related_model in predecessors_list_related_model:
            predecessors_list.append(item_predecesor_related_model.parent_task_id.id)
        #check for add new task to predecessor
        if list(set(predecessors_gantt_list) - set(predecessors_list)) != []:
            predecessors_update = list(set(predecessors_gantt_list) - set(predecessors_list))
            for item_for_write in predecessors_update:
                return_for_write.append((0,0,{'parent_task_id':item_for_write,'task_id':item['id'],
                                                       'type':'FS',
                                                       'lag_type':'day'}))
        #check for delete
        if list(set(predecessors_list) - set(predecessors_gantt_list)) != []:
            predecesors_update = list(set(predecessors_list) - set(predecessors_gantt_list))
            for item_for_delete in predecesors_update:
                select_item = request.env['project.task.predecessor'].search([('task_id','=',item['id']),
                                                                              ('parent_task_id','=',item_for_delete)])
                return_for_write.append((3,select_item.id))
            
#             print('New predecessors',predecessors_gantt_list)
#         print('From database:',predecessors_list,item['number'],'From gantt:',predecessors_gantt_list)
        return return_for_write    
    
    
    @http.route(['/gantt/ajax/save'], type='json', auth='public')
    def save_gantt(self,data):
        print('TEST INPUT',data['tasks'])
        #slit json to parts: project, new tasks, tasks for delete, depends dict
        #data list is separate dict item
        delete_list = data['deletedTaskIds'] #id of tasks for delete
        
        #dict item tasks
        depends_dict = {}
        project_list = [] #update for project
        tasks_for_update = [] 
        new_tasks_list = [] #new tasks data
        level_dict = {}
        
        for item_select in data['tasks']:
            #--------------------------------------------- prevent project error
            if 'project' not in item_select:
                item_select.update({'project':False})
            level_dict.update({str(item_select['level']):item_select['id']})    
            #depends dict number:id for fast finding of id by number
            if 'project' in item_select:
                if item_select['project'] == False:
                    if 'number' in item_select:
                        depends_dict.update({str(item_select['number']):item_select['id']})
            
            #--------------------------------------------------------- new tasks
            if str(item_select['id']).find('tmp_') == 0:
                if str(item_select['level']-1) in level_dict:
                    level_before = item_select['level']-1
                    if level_before != 0:
                        parent_id = level_dict[str(level_before)]
                    else:
                        parent_id = False
                    inheritance = {'parent_id':parent_id,
                                   'project_id':level_dict['0']-1000,
                                   'internal_id':item_select['id']}
                else:
                    inheritance = {'parent_id':False,
                                   'project_id':level_dict['0']-1000,
                                   'internal_id':item_select['id']}
                item_select.update(inheritance)
                new_tasks_list.append(item_select)
            
            #------------------------------------------------------ project list
            if 'project' in item_select:
                if item_select['project'] == True:
                        project_list.append(item_select)
            #--------------------------------------------- tasks for update list
            if str(item_select['id']).find('tmp_') == -1 and item_select['project'] is False:
                if str(item_select['level']-1) in level_dict:
                    level_before = item_select['level']-1
                    if level_before != 0:
                        parent_id = level_dict[str(level_before)]
                    else:
                        parent_id = False
                    inheritance = {'parent_id':parent_id,
                                   'project_id':level_dict['0']-1000}
                else:
                    inheritance = {'parent_id':False,
                                   'project_id':level_dict['0']-1000}
                item_select.update(inheritance)
                tasks_for_update.append(item_select)
                
        print('Delete:',delete_list)
        print('Depends dict',depends_dict)
        print('tasks for update:',tasks_for_update)
        print('new tasks:',new_tasks_list)    
        
        #=======================================================================
        # check and update only when record is changed
        #=======================================================================
        for item_update in tasks_for_update:
            current_task =  request.env['project.task'].search([('id','=',item_update['id'])])   
            start_date = datetime.datetime.utcfromtimestamp(item_update['start']/1000).strftime('%Y-%m-%d %H:%M:%S')
            end_date = datetime.datetime.utcfromtimestamp(item_update['end']/1000).strftime('%Y-%m-%d %H:%M:%S')
            predecessor_ids =  self.write_predecessors_number(item_update, depends_dict)
            print('check name',item_update['name'])
            if current_task.name == item_update['name']  \
                and start_date == current_task.date_start \
                and predecessor_ids == []:
                #and current_task.number == item_update['number']:
                pass
            else:
                current_task \
                      .with_context(for_record_dict = item_update) \
                      .write({'date_start':start_date,
                                    'number':item_update['number'],
                                    'predecessor_ids':predecessor_ids,
                                    'name':item_update['name']})
            if current_task.number != item_update['number']:
                current_task.with_context(only_number=True).write({
                                    'number':item_update['number']
                                    })
        #=======================================================================
        # create new tasks        
        #=======================================================================
        for item_create in new_tasks_list:
            start_date = datetime.datetime.utcfromtimestamp(item_create['start']/1000).strftime('%Y-%m-%d %H:%M:%S')
            end_date = datetime.datetime.utcfromtimestamp(item_create['end']/1000).strftime('%Y-%m-%d %H:%M:%S')
#                 #predecessor_ids = self.write_predecessors_number(item_to_save, data['tasks'])
            print('testcreate',item_create)
            res = request.env['project.task'] \
                     .with_context(for_record_dict=item_create) \
                     .create({'date_start':start_date,
                              'date_end':end_date,
                              'number':item_create['number'],
                              'project_id':item_create['project_id'],
                              'parent_id':item_create['parent_id'],
                              'name':item_create['name']})
        #=======================================================================
        # delete messages
        #=======================================================================
        for item_delete in delete_list:
            get_task = request.env['project.task'].search([('id','=',item_delete)])
            print('test delete:',get_task.name)
            if get_task:
                get_task.with_context(for_record_dict=item_delete).unlink()
              
    @http.route(['/gantt/ajax/tooltip'], type='json', auth="public")
    def get_tooltip(self,taskid):
        task = request.env['project.task'].sudo().search([('id','=',int(taskid))])
        result = task.get_info_variables()
        return result
           
    
    @http.route(['/gantt/ajax/<int:id>'], type='json', auth="public")
    def get_tasks_json(self,data,id):
        false = "false"
        true = "true"
        collapsed_list = []
        
        task_list = []
        task_dict = {}
        task_task = {}
        ret= { "selectedRow": 2, "deletedTaskIds": [],
                  "resources": [
                  {"id": "tmp_1", "name": "Resource 1"},
                  {"id": "tmp_2", "name": "Resource 2"},
                  {"id": "tmp_3", "name": "Resource 3"},
                  {"id": "tmp_4", "name": "Resource 4"}
                     ],
                  "roles":       [
                  {"id": "tmp_1", "name": "Project Manager"},
                  {"id": "tmp_2", "name": "Worker"},
                  {"id": "tmp_3", "name": "Stakeholder"},
                  {"id": "tmp_4", "name": "Customer"}
                ], "canWrite":    true, "canDelete":true, "canWriteOnParent": true, "canAdd":true}
        projects = request.env['project.project'].sudo().search([('id','=',id)])
        i = 0
        list_count = 0
        for project_item in projects:
            list_count += 1
            #get all tasks without parent task
#             if i == 1:
#                 break
            
            start_date = int(datetime.datetime.strptime(project_item.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            end_date = int(datetime.datetime.strptime(project_item.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            task_task = {"id":project_item.id+1000,
                         "name":project_item.name,
                         "level":0,
                         "hasChild":True,
                         "progress": 0, 
                        "progressByWorklog": False,
                        "relevance": 0, 
                        "type": "",
                         "typeId": "",
                         "description": "", 
                        "code": "", 
                        "status": 
                        "STATUS_ACTIVE", 
                        "depends": [], 
                        "canWrite": True, 
                        "start": start_date, 
                        "duration": 20, 
                        "end": end_date, 
                        "startIsMilestone": False, 
                        "endIsMilestone": False,
                         "collapsed": True, 
                        "assigs": [],
                        "project":True,
                        "project_id":project_item.id,
                        "parent_id":False}
            task_list.append(task_task)
            collapsed_list.append(project_item.id+1000)
            i = i+1
            get_tasks_without_parent = request.env['project.task'].sudo().search([('parent_id','=',False),
                                                                                  ('project_id','=',project_item.id)])
            
            for item_task in get_tasks_without_parent:
                list_count += 1
                start_date = int(datetime.datetime.strptime(item_task.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
                end_date = int(datetime.datetime.strptime(item_task.date_end, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
                task_dict = {"id":item_task.id,
                             "name":item_task.name,
                             "level":1,
                             "hasChild":True,
                             "progress": 0, 
                            "progressByWorklog": False,
                            "relevance": 0, 
                            "type": "",
                             "typeId": "",
                             "description": item_task.description, 
                            "code": "", 
                            "status": 
                            "STATUS_ACTIVE", 
                            "depends": item_task.predecessor_ids.ids,
                            "canWrite": True, 
                            "start": start_date, 
                            "duration": item_task.duration/86400, 
                            "end": end_date, 
                            "startIsMilestone": item_task.is_milestone, 
                            "endIsMilestone": False,
                             "collapsed": True, 
                            "assigs": [],
                            "project":False,
                            "project_id":project_item.id,
                            "parent_id":False}
                task_list.append(task_dict)
                if item_task.child_ids.ids != []:
                    subtask_list = request.env['project.task'].sudo().search([('parent_id','=',item_task.id)])
                    for subtask_list_item in subtask_list:
                        list_count += 1
                        
                        start_date = int(datetime.datetime.strptime(item_task.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
                        end_date = int(datetime.datetime.strptime(item_task.date_end, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
                        task_dict = {"id":subtask_list_item.id,
                             "name":subtask_list_item.name,
                             "level":2,
                             "hasChild":True,
                             "progress": 0, 
                            "progressByWorklog": False,
                            "relevance": 0, 
                            "type": "",
                             "typeId": "",
                             "description": subtask_list_item.description, 
                            "code": "", 
                            "status": 
                            "STATUS_ACTIVE", 
                            "depends": subtask_list_item.predecessor_ids.ids,
                            "canWrite": True, 
                            "start": start_date, 
                            "duration": subtask_list_item.duration/86400, 
                            "end": end_date, 
                            "startIsMilestone": subtask_list_item.is_milestone, 
                            "endIsMilestone": False,
                             "collapsed": True, 
                            "assigs": [],
                            "project":False,
                            "project_id":project_item.id,
                            "parent_id":subtask_list_item.parent_id.id}
                        task_list.append(task_dict)
        ret= { "selectedRow": 2, "deletedTaskIds": [],
                  "resources": [
                  {"id": "tmp_1", "name": "Resource 1"},
                  {"id": "tmp_2", "name": "Resource 2"},
                  {"id": "tmp_3", "name": "Resource 3"},
                  {"id": "tmp_4", "name": "Resource 4"}
                ],
                  "roles":       [
                  {"id": "tmp_1", "name": "Project Manager"},
                  {"id": "tmp_2", "name": "Worker"},
                  {"id": "tmp_3", "name": "Stakeholder"},
                  {"id": "tmp_4", "name": "Customer"}
                ], "canWrite":    true, "canDelete":true, "canWriteOnParent": true, "canAdd":true}
        
        list_after_dependes = []
        list_with_numbers = []
        count_number = 1
        for item_list_with_number in task_list:
            item_list_with_number.update({'number':count_number})
            list_with_numbers.append(item_list_with_number)
            count_number += 1
        
        #change list with id to numbers        
        for item_task_list in list_with_numbers:
            depends_ids = item_task_list['depends']
            if depends_ids != []:
                new_depends = []
                for item_in_depends_list in depends_ids:
                    get_depends = str(self.get_number_of_depends(item_in_depends_list,list_with_numbers))
                    new_depends.append(get_depends)
                item_task_list.update({"depends":",".join(new_depends)})
            else:
                item_task_list.update({"depends":""})
            list_after_dependes.append(item_task_list)
        
#         for item_test in list_after_dependes:
#             print(item_test['name'],' depends:',item_test['depends'],"id:",item_test['id']," number:",item_test['number'])
                    
        ret.update({"tasks":list_after_dependes,"collapsed":collapsed_list})
        
        #print(json.dumps(ret))
        return ret
    
    