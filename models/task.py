# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import UserError, AccessError

class GanttViewTask(models.Model):
    
    _inherit = 'project.task'
    
    number = fields.Integer(string="Gantt number")
    
    @api.one
    def write(self,vals):
        super(GanttViewTask,self).write(vals)
        if 'send_message' not in self._context and 'only_number' not in self._context:
            start_date = int(datetime.strptime(self.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            end_date = int(datetime.strptime(self.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            for_record_dict = self._context['for_record_dict']
            message_dict = {'id':self.id,
                            'name':self.name,
                            'start':start_date,
                            'end':end_date,
                            'depends':for_record_dict['depends'],
                            'action':'write'}
            (channel, message) = ((self._cr.dbname, 'project.task', 7), (message_dict))
            self.env['bus.bus'].sendone(channel, message)
            self = self.with_context(send_message=True)
        return True
    
    @api.model
    def create(self,vals):
        res = super(GanttViewTask,self).create(vals)
        print('TEST CREATE')
        if 'for_record_dict' in res._context:
            start_date = int(datetime.strptime(res.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            end_date = int(datetime.strptime(res.date_start, '%Y-%m-%d %H:%M:%S').strftime("%s"))*1000
            record_context_dict = res._context['for_record_dict']
            message_dict = {"id":res.id,
                       "name":res.name,
                       "number":record_context_dict['number'],
                       "level":record_context_dict['level'],
                       "hasChild":False,
                         "progress": 0, 
                        "progressByWorklog": record_context_dict['progressByWorklog'],
                        "relevance": record_context_dict['relevance'], 
                        "type": record_context_dict['type'],
                         "typeId": record_context_dict['typeId'],
                         "description": record_context_dict['description'], 
                        "code": record_context_dict['code'], 
                        "status": record_context_dict['status'], 
                        "depends": record_context_dict['depends'], 
                        "canWrite": record_context_dict['canWrite'], 
                        "start": start_date, 
                        "duration": record_context_dict['duration'], 
                        "end": end_date, 
                        "startIsMilestone": record_context_dict['startIsMilestone'], 
                        "endIsMilestone": record_context_dict['endIsMilestone'],
                         "collapsed": record_context_dict['collapsed'], 
                        "assigs": [],
                        "project":False,
                        "project_id":res.project_id.id,
                        "parent_id":res.parent_id.id,
                        "action":"create",
                        "internal_id":record_context_dict['internal_id']}
            print(message_dict)
            (channel, message) = ((self._cr.dbname, 'project.task', 7), (message_dict))
            self.env['bus.bus'].sendone(channel, message)
        return res
    
    def unlink(self):
        super(GanttViewTask,self).unlink()
        delete_dict = {'id':self.id,'action':'delete'}
        (channel, message) = ((self._cr.dbname, 'project.task', 7), (delete_dict))
        print(channel,message)
        self.env['bus.bus'].sendone(channel, message)
        return True
class GanttViewProject(models.Model):
    
    _inherit = 'project.project'
    
    number = fields.Integer(string="Number")
