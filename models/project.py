# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import UserError, AccessError

class GanttViewProject(models.Model):
    
    _inherit = "project.project"
    
    current_id = fields.Char(compute="_get_id_for_kanban")
    
    @api.one
    def _get_id_for_kanban(self):
        self.current_id = self.id